package com.example.alibabatestproject

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AlibabaApplication : Application() {
}