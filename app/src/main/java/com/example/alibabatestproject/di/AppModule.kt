package com.example.alibabatestproject.di

import com.example.alibabatestproject.data.error.GitHubErrorHandler
import com.example.alibabatestproject.data.error.GitHubGeneralErrorHandlerImpl
import com.example.alibabatestproject.data.repository.GitHubRepositoryImp
import com.example.alibabatestproject.di.qualifier.IoDispatcherQualifier
import com.example.alibabatestproject.di.qualifier.MainDispatcherQualifier
import com.example.alibabatestproject.data.repository.GitHubRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideErrorHandler(imp : GitHubGeneralErrorHandlerImpl) : GitHubErrorHandler = imp

    @Provides
    @Singleton
    @IoDispatcherQualifier
    fun provideIoDispatcher(): CoroutineDispatcher {
        return Dispatchers.IO
    }

    @Provides
    @Singleton
    @MainDispatcherQualifier
    fun provideMainDispatcher(): CoroutineDispatcher {
        return Dispatchers.Main
    }

    @Provides
    @Singleton
    fun provideGithubRepository(gitHubRepositoryImp: GitHubRepositoryImp) : GitHubRepository = gitHubRepositoryImp
}