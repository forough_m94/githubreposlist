package com.example.alibabatestproject.di

import android.content.Context
import androidx.room.Room
import com.example.alibabatestproject.data.source.local.AppDatabase
import com.example.alibabatestproject.data.source.local.GitHubDao
import com.example.alibabatestproject.data.source.local.GitHubLocalDataSource
import com.example.alibabatestproject.data.source.local.GithubLocalDataSourceImp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "githubdb.db"
        ).build()
    }

    @Provides
    @Singleton
    fun provideGitHubDao(appDatabase: AppDatabase): GitHubDao {
        return appDatabase.gitHubDao()
    }

    @Provides
    @Singleton
    fun provideLocalDataSource(imp: GithubLocalDataSourceImp): GitHubLocalDataSource = imp

}