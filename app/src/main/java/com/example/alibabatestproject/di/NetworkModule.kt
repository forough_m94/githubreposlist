package com.example.alibabatestproject.di

import com.example.alibabatestproject.api.GitHubApi
import com.example.alibabatestproject.data.source.network.GitHubNetworkDataSource
import com.example.alibabatestproject.data.source.network.GitHubNetworkDataSourceImp
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.Duration
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun provideBaseUrl() = GitHubApi.GitHubBaseURL

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient
            .Builder()
            .addInterceptor(loggingInterceptor)
            .connectTimeout(5000, TimeUnit.MILLISECONDS)
            .build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, BASE_URL: String): Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .build()

    @Provides
    @Singleton
    fun provideGitHubApi(retrofit: Retrofit) = retrofit.create(GitHubApi::class.java)

    @Provides
    @Singleton
    fun provideNetworkDataSource(gitHubNetworkDataSourceImp: GitHubNetworkDataSourceImp): GitHubNetworkDataSource =
        gitHubNetworkDataSourceImp

}