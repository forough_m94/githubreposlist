package com.example.alibabatestproject.ui.repos

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.alibabatestproject.R
import com.example.alibabatestproject.data.model.GitHubRepoSummaryObject
import com.example.alibabatestproject.databinding.RepositoryListFragmentBinding
import com.example.alibabatestproject.util.EndlessRecyclerOnScrollListener
import com.example.alibabatestproject.util.SortType
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GitHubReposFragment : Fragment() {

    private val viewModel: GitHubReposViewModel by viewModels()
    private var githubRepos: ArrayList<GitHubRepoSummaryObject>? = null
    private val endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener by lazy {
        object : EndlessRecyclerOnScrollListener(
            githubListFragmentBinding.githubReposRv.layoutManager as LinearLayoutManager,
            ITEMS_PER_PAGE
        ) {
            override fun onLoadMore(currentPage: Int) {
                Log.d("forough", "loadMore")
                viewModel.requestGitHubRepositories(currentPage, ITEMS_PER_PAGE, sortType, false)
            }

        }.apply {
            visibleThreshold = ITEMS_PER_PAGE / 2
        }
    }

    companion object {
        const val ITEMS_PER_PAGE = 20
    }

    private var sortType = SortType.STARS


    private lateinit var githubListFragmentBinding: RepositoryListFragmentBinding
    private val githubReposAdapter: GithubReposAdapter by lazy {
        GithubReposAdapter { repoId, ownerId ->
            findNavController().navigate(
                GitHubReposFragmentDirections.actionGitHubReposFragmentToGithubRepoDetailsFragment(
                    repoId, ownerId
                )
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        sharedPref?.let {
            val sortSavedName =
                sharedPref.getString(getString(R.string.prefSortName), sortType.sortName)
            SortType.values().forEach { if (it.sortName == sortSavedName) sortType = it }
        }
        githubListFragmentBinding = RepositoryListFragmentBinding.inflate(inflater)
        setupGithubReposRv()
        setupSortBtn()
        viewModel.requestGitHubRepositories(1, ITEMS_PER_PAGE, sortType, false)
        return githubListFragmentBinding.root
    }

    private fun setupSortBtn() {
        githubListFragmentBinding.sortType = sortType
        githubListFragmentBinding.sortBtn.setOnClickListener {
            sortType = if (sortType == SortType.STARS) {
                SortType.FORKS
            } else {
                SortType.STARS
            }
            githubListFragmentBinding.sortType = sortType
            refreshGithubReposRv()
        }
    }

    private fun refreshGithubReposRv() {
        githubRepos?.size?.let { githubReposAdapter.notifyItemRangeRemoved(0, it) }
        endlessRecyclerOnScrollListener.refresh()
        githubRepos?.clear()
        viewModel.requestGitHubRepositories(1, ITEMS_PER_PAGE, sortType, true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        viewModel.githubReposLiveData.observe(viewLifecycleOwner) {
            if (githubRepos == null) {
                githubRepos = arrayListOf()
            }
            githubRepos?.addAll(it)
            githubReposAdapter.githubRepos = githubRepos
            githubRepos?.size?.let { newSize ->
                githubReposAdapter.notifyItemRangeInserted(
                    newSize - it.size,
                    newSize
                )
            }
        }

        viewModel.loadingLiveData.observe(viewLifecycleOwner) {
            githubListFragmentBinding.loading.visibility = if (it) VISIBLE else GONE
        }

        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            githubListFragmentBinding.error = it
            endlessRecyclerOnScrollListener.tryAgain()
        }
    }

    private fun setupGithubReposRv() {
        val githubReposRv = githubListFragmentBinding.githubReposRv
        githubReposRv.layoutManager = LinearLayoutManager(context)
        githubReposRv.adapter = githubReposAdapter
        githubReposRv.addOnScrollListener(endlessRecyclerOnScrollListener)
    }

    override fun onStop() {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        sharedPref?.edit()?.let {
            it.putString(getString(R.string.prefSortName), sortType.sortName)
            it.apply()
        }
        super.onStop()
    }

}
