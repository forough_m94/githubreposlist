package com.example.alibabatestproject.ui.repos

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.alibabatestproject.databinding.RepositoryItemBinding
import com.example.alibabatestproject.data.model.GitHubRepoSummaryObject

class GithubReposAdapter(val onItemClicked: ((githubRepoId: Int, repoOwnerId: Int) -> Unit)) :
    RecyclerView.Adapter<GithubReposAdapter.GithubRepoViewHolder>() {

    var githubRepos: List<GitHubRepoSummaryObject>? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GithubRepoViewHolder {
        return GithubRepoViewHolder(
            RepositoryItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: GithubRepoViewHolder, position: Int) {
        githubRepos?.get(position)?.let {
            holder.itemView.tag = position
            holder.bind(it)
        }
    }

//    private class GitHubDiffUtil : DiffUtil.ItemCallback<GitHubRepoSummaryObject>() {
//        override fun areItemsTheSame(
//            oldItem: GitHubRepoSummaryObject,
//            newItem: GitHubRepoSummaryObject
//        ): Boolean {
//            return oldItem.id == newItem.id
//        }
//
//        override fun areContentsTheSame(
//            oldItem: GitHubRepoSummaryObject,
//            newItem: GitHubRepoSummaryObject
//        ): Boolean {
//            return oldItem == newItem
//        }
//    }

    inner class GithubRepoViewHolder(private val binding: RepositoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                val item = githubRepos?.get(it.tag as Int)
                item?.let {
                    onItemClicked.invoke(
                        item.id, item.ownerId
                    )
                }
            }
        }

        fun bind(repo: GitHubRepoSummaryObject) {
            binding.githubRepo = repo
            binding.executePendingBindings()
        }
    }

    override fun getItemCount(): Int {
        return githubRepos?.size ?: 0
    }

}