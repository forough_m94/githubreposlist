package com.example.alibabatestproject.ui.repos

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.alibabatestproject.data.model.GitHubRepoSummaryObject
import com.example.alibabatestproject.data.repository.GitHubRepository
import com.example.alibabatestproject.util.GitHubResult
import com.example.alibabatestproject.util.SortType
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GitHubReposViewModel @Inject constructor(
    private val gitHubRepository: GitHubRepository,
    @ApplicationContext private val context: Context
) : ViewModel() {

    private val _loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val loadingLiveData: LiveData<Boolean> = _loadingLiveData

    private val _errorLiveData: MutableLiveData<String> = MutableLiveData()
    val errorLiveData: LiveData<String> = _errorLiveData

    private val _githubReposLiveData: MutableLiveData<List<GitHubRepoSummaryObject>> by lazy {
        MutableLiveData<List<GitHubRepoSummaryObject>>()
    }
    val githubReposLiveData: LiveData<List<GitHubRepoSummaryObject>> =
        _githubReposLiveData

    fun requestGitHubRepositories(
        page: Int,
        itemsPerPage: Int,
        sortType: SortType,
        needRefresh: Boolean
    ) {
        viewModelScope.launch {
            gitHubRepository.getGitHubRepositories(page, itemsPerPage, sortType, needRefresh)
                .collect {
                    when (it) {
                        is GitHubResult.Error -> {
                            _loadingLiveData.value = false
                            _errorLiveData.value = it.error.toErrorMessage(context)
                        }
                        GitHubResult.Loading -> {
                            _loadingLiveData.value = true
                        }
                        is GitHubResult.Success -> {
                            _loadingLiveData.value = false
                            _githubReposLiveData.value = it.data
                        }
                    }
                }
        }
    }

}