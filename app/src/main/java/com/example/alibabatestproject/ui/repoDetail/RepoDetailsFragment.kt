package com.example.alibabatestproject.ui.repoDetail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.alibabatestproject.databinding.RepoDetailsFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class RepoDetailsFragment : Fragment() {

    private lateinit var viewBinding: RepoDetailsFragmentBinding
    private val navArgs: RepoDetailsFragmentArgs by navArgs()

    @Inject
    lateinit var assistedFactory: RepoDetailsViewModel.AssistedFactory

    private val viewModel: RepoDetailsViewModel by viewModels {
        RepoDetailsViewModel.provideFactory(assistedFactory, navArgs.githubRepoId)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = RepoDetailsFragmentBinding.inflate(inflater)
        viewBinding.openUrlBtn.setOnClickListener {
            openBrowserIntent(viewBinding.githubRepo?.htmlUrl)
        }
        return viewBinding.root
    }

    private fun openBrowserIntent(url: String?) {
        url.let {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        viewModel.repoDetailDetailsLiveData.observe(viewLifecycleOwner) {
            viewBinding.githubRepo = it
        }

        viewModel.contributorsLiveData.observe(viewLifecycleOwner) {
            viewBinding.contributors = it
        }

        viewModel.reposLoadingLiveData.observe(viewLifecycleOwner) {
            viewBinding.detailsLoading.visibility = if (it) VISIBLE else GONE
        }

        viewModel.usersLoadingLiveData.observe(viewLifecycleOwner) {
            viewBinding.contributorsLoading.visibility = if (it) VISIBLE else GONE
        }

        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            viewBinding.error = it
        }
    }
}