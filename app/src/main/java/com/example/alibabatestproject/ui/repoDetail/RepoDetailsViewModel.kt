package com.example.alibabatestproject.ui.repoDetail

import android.content.Context
import androidx.lifecycle.*
import com.example.alibabatestproject.data.model.GitHubRepoDetailObject
import com.example.alibabatestproject.data.model.GitHubUser
import com.example.alibabatestproject.data.repository.GitHubRepository
import com.example.alibabatestproject.util.GitHubResult
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class RepoDetailsViewModel @AssistedInject constructor(
    private val gitHubRepository: GitHubRepository,
    @Assisted val repoId: Int,
    @ApplicationContext private val context: Context
) :
    ViewModel() {

    private val _reposLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val reposLoadingLiveData: LiveData<Boolean> = _reposLoadingLiveData

    private val _usersLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val usersLoadingLiveData: LiveData<Boolean> = _usersLoadingLiveData

    private val _errorLiveData: MutableLiveData<String> = MutableLiveData()
    val errorLiveData: LiveData<String> = _errorLiveData

    private val _repoDetailsLiveData = MutableLiveData<GitHubRepoDetailObject>()
    val repoDetailDetailsLiveData: LiveData<GitHubRepoDetailObject> = _repoDetailsLiveData

    private val _contributorsLiveData = MutableLiveData<List<GitHubUser>>()
    val contributorsLiveData: LiveData<List<GitHubUser>> = _contributorsLiveData

    init {
        requestRepoDetail()
    }

    private fun requestRepoDetail() {
        viewModelScope.launch {
            gitHubRepository.getGitHubRepository(repoId).collect {
                when (it) {
                    is GitHubResult.Error -> {
                        _reposLoadingLiveData.value = false
                        _errorLiveData.value = it.error.toErrorMessage(context)
                    }
                    GitHubResult.Loading -> {
                        _reposLoadingLiveData.value = true
                    }
                    is GitHubResult.Success -> {
                        _reposLoadingLiveData.value = false
                        _repoDetailsLiveData.value = it.data
                        requestContributors(it.data.name, it.data.owner.name);
                    }
                }
            }
        }
    }

    private fun requestContributors(repoName: String, ownerName: String) {
        viewModelScope.launch {
            gitHubRepository.getContributors(repoId, repoName, ownerName).collect {
                when (it) {
                    is GitHubResult.Error -> {
                        _usersLoadingLiveData.value = false
                        _errorLiveData.value = it.error.toErrorMessage(context)
                    }
                    GitHubResult.Loading -> _usersLoadingLiveData.value = true
                    is GitHubResult.Success -> {
                        _usersLoadingLiveData.value = false
                        _contributorsLiveData.value = it.data
                    }
                }
            }
        }
    }

    @dagger.assisted.AssistedFactory
    interface AssistedFactory {
        fun create(repoId: Int): RepoDetailsViewModel
    }

    companion object {
        fun provideFactory(
            assistedFactory: AssistedFactory,
            repoId: Int
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return assistedFactory.create(repoId) as T
            }
        }
    }
}