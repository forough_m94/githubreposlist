package com.example.alibabatestproject.util

import com.example.alibabatestproject.data.error.GitHubErrorEntity

sealed class GitHubResult<out T> {

    data class Success<out T>(val data: T) : GitHubResult<T>()

    data class Error<T>(val error: GitHubErrorEntity) : GitHubResult<T>()

    object Loading : GitHubResult<Nothing>()

}