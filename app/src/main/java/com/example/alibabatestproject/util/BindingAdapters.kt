package com.example.alibabatestproject.util

import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.BindingAdapter
import com.example.alibabatestproject.R
import com.example.alibabatestproject.data.model.GitHubUser

@BindingAdapter("ownerName")
fun setOwnerName(textView: TextView, name: String?) {
    name?.let {
        textView.text =
            String.format(textView.context.getString(R.string.repoOwnerNamePattern), name)
    }
}

@BindingAdapter("contributorsList")
fun setContributors(textView: TextView, users: List<GitHubUser>?) {
    users?.let {
        val names = StringBuilder()
        users.forEach {
            names.append(it.name + ", ")
        }
        textView.text = names.substring(0, names.length - 2)
    }
}

@BindingAdapter("sortText")
fun setSortedText(textView: TextView, sortType: SortType) {
    if (sortType == SortType.STARS) {
        textView.text = String.format(
            textView.context.getString(R.string.sortText),
            SortType.STARS.sortName,
            SortType.FORKS.sortName
        )
    } else {
        textView.text = String.format(
            textView.context.getString(R.string.sortText),
            SortType.FORKS.sortName,
            SortType.STARS.sortName
        )
    }
}

@BindingAdapter("setError")
fun setErrorMessage(viewGroup: ViewGroup, message: String?) {
    message?.let {
        Toast.makeText(viewGroup.context, message, Toast.LENGTH_SHORT).show()
    }
}
