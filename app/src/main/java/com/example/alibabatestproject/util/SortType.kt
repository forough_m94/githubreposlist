package com.example.alibabatestproject.util

enum class SortType(val sortName: String) {
    STARS("stars"),
    FORKS("forks");
}