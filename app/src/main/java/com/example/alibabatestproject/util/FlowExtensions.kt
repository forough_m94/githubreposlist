package com.example.alibabatestproject.util

import com.example.alibabatestproject.data.error.GitHubErrorHandler
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

fun <T> Flow<T>.toResult(errorHandler: GitHubErrorHandler): Flow<GitHubResult<T>> = this
    .map<T, GitHubResult<T>> {
        GitHubResult.Success(it)
    }.onStart {
        emit(GitHubResult.Loading)
    }.catch {
        emit(GitHubResult.Error(errorHandler.getError(it)))
    }