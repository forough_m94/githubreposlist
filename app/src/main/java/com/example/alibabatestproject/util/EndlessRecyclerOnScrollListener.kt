package com.example.alibabatestproject.util

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


abstract class EndlessRecyclerOnScrollListener(
    private val mLinearLayoutManager: LinearLayoutManager,
    pageSize: Int = 5
) : RecyclerView.OnScrollListener() {

    private var firstVisibleItem: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var currentPage = 1

    /**
    The total number of items in the dataset after the last load
     */
    private var previousTotal = 0

    /**
     * True if we are still waiting for the last set of data to load.
     */
    private var loading = true

    /**
     * The minimum amount of items to have below your current scroll position before loading more.
     */
    var visibleThreshold = pageSize

    fun refresh() {
        currentPage = 1
        previousTotal = 0
        loading = true
    }

    fun tryAgain() {
        loading = false
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        visibleItemCount = recyclerView.childCount
        totalItemCount = mLinearLayoutManager.itemCount
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition()

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }

        if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) { // End has been reached
            currentPage++
            onLoadMore(currentPage)
            loading = true
        }
    }

    abstract fun onLoadMore(currentPage: Int)
}
