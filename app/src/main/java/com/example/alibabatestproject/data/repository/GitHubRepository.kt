package com.example.alibabatestproject.data.repository

import com.example.alibabatestproject.util.GitHubResult
import com.example.alibabatestproject.data.model.GitHubRepoDetailObject
import com.example.alibabatestproject.data.model.GitHubRepoSummaryObject
import com.example.alibabatestproject.data.model.GitHubUser
import com.example.alibabatestproject.util.SortType
import kotlinx.coroutines.flow.Flow

interface GitHubRepository {

    fun getGitHubRepositories(
        page: Int,
        itemsPerPage: Int,
        sortType: SortType,
        needRefresh: Boolean
    ): Flow<GitHubResult<List<GitHubRepoSummaryObject>>>

    suspend fun deleteGithubRepositories()

    fun getGitHubRepository(repoId: Int): Flow<GitHubResult<GitHubRepoDetailObject>>

    fun getContributors(
        repoId: Int, repoName: String, ownerName: String
    ): Flow<GitHubResult<List<GitHubUser>>>
}