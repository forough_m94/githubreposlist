package com.example.alibabatestproject.data.error

interface GitHubErrorHandler {
    fun getError(throwable: Throwable): GitHubErrorEntity
}