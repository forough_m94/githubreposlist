package com.example.alibabatestproject.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.alibabatestproject.data.source.local.model.GithubRepoRoomModel
import com.example.alibabatestproject.data.source.local.model.UserRepoJoin
import com.example.alibabatestproject.data.source.local.model.UserRoomModel

@Dao
interface GitHubDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAllUsers(users: List<UserRoomModel>)

    @Query("SELECT * FROM UserRoomModel WHERE id = :userId")
    suspend fun getUser(userId: Int): UserRoomModel

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAllRepositories(gitHubRepos: List<GithubRepoRoomModel>)

    @Query("SELECT * FROM GithubRepoRoomModel ORDER BY starsCount DESC LIMIT :itemsPerPage OFFSET :page*:itemsPerPage ")
    suspend fun getGitHubReposStarSort(
        page: Int,
        itemsPerPage: Int
    ): List<GithubRepoRoomModel>

    @Query("SELECT * FROM GithubRepoRoomModel ORDER BY forksCount DESC LIMIT :itemsPerPage OFFSET :page*:itemsPerPage ")
    suspend fun getGitHubReposForkSort(
        page: Int,
        itemsPerPage: Int
    ): List<GithubRepoRoomModel>

    @Query("SELECT * FROM GithubRepoRoomModel WHERE id = :repoId")
    suspend fun getGitHubRepository(repoId: Int): GithubRepoRoomModel

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAllUserRepoJoins(userRepoJoin: List<UserRepoJoin>)

    @Query("SELECT * FROM UserRoomModel INNER JOIN UserRepoJoin ON UserRoomModel.id = UserRepoJoin.userId WHERE UserRepoJoin.repoId =:repoId")
    suspend fun getUsersInRepository(repoId: Int): List<UserRoomModel>

    @Query("DELETE FROM GithubRepoRoomModel")
    suspend fun deleteGithubRepositories()

    @Query("DELETE FROM UserRoomModel")
    suspend fun deleteUsers()

}