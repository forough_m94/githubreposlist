package com.example.alibabatestproject.data.model

data class GitHubUser(
    val id: Int,
    val name: String,
    val avatarUrl: String
)