package com.example.alibabatestproject.data.source.network

import com.example.alibabatestproject.api.GitHubApi
import com.example.alibabatestproject.data.source.network.model.GitHubRepoRemoteModel
import com.example.alibabatestproject.data.source.network.model.GitHubUserRemoteModel
import com.example.alibabatestproject.util.SortType
import javax.inject.Inject

class GitHubNetworkDataSourceImp @Inject constructor(
    private val gitHubApi: GitHubApi
) : GitHubNetworkDataSource {

    override suspend fun getGitHubRepositories(
        page: Int,
        itemsPerPage: Int,
        sortType: SortType
    ): List<GitHubRepoRemoteModel> {
        return gitHubApi.getGitHubRepos(sortType.sortName, page, itemsPerPage).items
    }

    override suspend fun getContributors(
        owner: String,
        repoName: String
    ): List<GitHubUserRemoteModel> {
        return gitHubApi.getRepositoryContributors(owner, repoName)
    }
}