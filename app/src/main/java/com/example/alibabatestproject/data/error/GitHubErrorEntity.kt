package com.example.alibabatestproject.data.error

import android.content.Context
import com.example.alibabatestproject.R

sealed class GitHubErrorEntity {

    object Network : GitHubErrorEntity()

    object NotFound : GitHubErrorEntity()

    object AccessDenied : GitHubErrorEntity()

    object ServiceUnavailable : GitHubErrorEntity()

    object Canceled : GitHubErrorEntity()

    data class Unknown(val message: String?) : GitHubErrorEntity()

    fun toErrorMessage(context: Context): String {
        return when (this) {
            AccessDenied -> context.getString(R.string.accessDenied)
            Canceled -> context.getString(R.string.requestCanceled)
            Network -> context.getString(R.string.networkError)
            NotFound -> context.getString(R.string.notFound)
            ServiceUnavailable -> context.getString(R.string.serviceUnavailable)
            is Unknown -> message
                ?: context.getString(R.string.unknownError)
        }
    }
}