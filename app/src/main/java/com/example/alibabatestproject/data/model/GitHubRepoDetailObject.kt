package com.example.alibabatestproject.data.model

data class GitHubRepoDetailObject(
    val id: Int,
    val name: String,
    val starsCount: Int,
    val forksCount: Int,
    val description: String,
    val htmlUrl: String,
    val owner: GitHubUser
)
