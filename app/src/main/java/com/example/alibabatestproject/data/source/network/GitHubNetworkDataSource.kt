package com.example.alibabatestproject.data.source.network

import com.example.alibabatestproject.data.source.network.model.GitHubRepoRemoteModel
import com.example.alibabatestproject.data.source.network.model.GitHubUserRemoteModel
import com.example.alibabatestproject.util.SortType

interface GitHubNetworkDataSource {

    suspend fun getGitHubRepositories(
        page: Int,
        itemsPerPage: Int,
        sortType: SortType
    ): List<GitHubRepoRemoteModel>

    suspend fun getContributors(
        owner: String,
        repoName: String
    ): List<GitHubUserRemoteModel>

}