package com.example.alibabatestproject.data.source.network.model

import com.example.alibabatestproject.data.model.GitHubRepoSummaryObject
import com.example.alibabatestproject.data.source.local.model.GithubRepoRoomModel
import com.google.gson.annotations.SerializedName

data class GitHubRepoRemoteModel(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String?,
    @SerializedName("stargazers_count") val starsCount: Int?,
    @SerializedName("forks_count") val forksCount: Int?,
    @SerializedName("description") val description: String?,
    @SerializedName("html_url") val htmlUrl: String?,
    @SerializedName("owner") val owner: GitHubUserRemoteModel,
    @SerializedName("contributors_url") val contributorsUrl: String?
)

fun GitHubRepoRemoteModel.toLocal() = GithubRepoRoomModel(
    id = id,
    name = name ?: "",
    starsCount = starsCount ?: 0,
    forksCount = forksCount ?: 0,
    description = description ?: "",
    htmlUrl = htmlUrl ?: "",
    ownerId = owner.id,
    contributorsUrl = contributorsUrl ?: ""
)

fun GitHubRepoRemoteModel.toDomain() = GitHubRepoSummaryObject(
    id = id,
    name = name ?: "",
    starsCount = starsCount ?: 0,
    forksCount = forksCount ?: 0,
    description = description ?: "",
    htmlUrl = htmlUrl ?: "",
    ownerId = owner.id
)