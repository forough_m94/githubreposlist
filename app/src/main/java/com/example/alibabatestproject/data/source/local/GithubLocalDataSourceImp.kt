package com.example.alibabatestproject.data.source.local

import com.example.alibabatestproject.data.source.local.model.GithubRepoRoomModel
import com.example.alibabatestproject.data.source.local.model.UserRepoJoin
import com.example.alibabatestproject.data.source.local.model.UserRoomModel
import com.example.alibabatestproject.util.SortType
import javax.inject.Inject

class GithubLocalDataSourceImp @Inject constructor(
    private val gitHubDao: GitHubDao
) : GitHubLocalDataSource {

    override suspend fun insertUsers(users: List<UserRoomModel>) {
        gitHubDao.insertAllUsers(users)
    }

    override suspend fun insertAllUserRepoJoins(userRepoJoins: List<UserRepoJoin>) {
        gitHubDao.insertAllUserRepoJoins(userRepoJoins)
    }

    override suspend fun getUsersInRepository(repoId: Int): List<UserRoomModel> {
        return gitHubDao.getUsersInRepository(repoId)
    }

    override suspend fun insertGithubRepositories(githubRepos: List<GithubRepoRoomModel>) {
        gitHubDao.insertAllRepositories(githubRepos)
    }

    override suspend fun getGithubRepositories(
        page: Int,
        itemsPerPage: Int,
        sortType: SortType
    ): List<GithubRepoRoomModel> {
        return if (sortType == SortType.STARS) {
            gitHubDao.getGitHubReposStarSort(page - 1, itemsPerPage)
        } else {
            gitHubDao.getGitHubReposForkSort(page - 1, itemsPerPage)
        }
    }

    override suspend fun getGithubRepository(repoId: Int): Pair<UserRoomModel, GithubRepoRoomModel> {
        val repo = gitHubDao.getGitHubRepository(repoId)
        val repoOwner = gitHubDao.getUser(repo.ownerId)
        return Pair(repoOwner, repo)
    }

    override suspend fun deleteUsers() {
        gitHubDao.deleteUsers()
    }

    override suspend fun deleteGithubRepositories() {
        gitHubDao.deleteGithubRepositories()
    }
}