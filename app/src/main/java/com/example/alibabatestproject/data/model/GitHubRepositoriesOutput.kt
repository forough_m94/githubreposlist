package com.example.alibabatestproject.data.model

import com.example.alibabatestproject.data.source.network.model.GitHubRepoRemoteModel

data class GitHubRepositoriesOutput(val items: List<GitHubRepoRemoteModel>)