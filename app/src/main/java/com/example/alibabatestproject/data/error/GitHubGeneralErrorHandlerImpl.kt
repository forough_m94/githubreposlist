package com.example.alibabatestproject.data.error

import retrofit2.HttpException
import java.io.IOException
import java.net.HttpURLConnection
import javax.inject.Inject

class GitHubGeneralErrorHandlerImpl @Inject constructor() : GitHubErrorHandler {

    override fun getError(throwable: Throwable): GitHubErrorEntity {
        return when (throwable) {
            is IOException -> GitHubErrorEntity.Network
            is HttpException -> {
                when (throwable.code()) {
                    // no cache found in case of no network, thrown by retrofit -> treated as network error
//                   UNSATISFIABLE_REQUEST -> ErrorEntity.Network

                    // not found
                    HttpURLConnection.HTTP_NOT_FOUND -> GitHubErrorEntity.NotFound

                    // access denied
                    HttpURLConnection.HTTP_FORBIDDEN -> GitHubErrorEntity.AccessDenied

                    // unavailable service
                    HttpURLConnection.HTTP_UNAVAILABLE -> GitHubErrorEntity.ServiceUnavailable

                    // all the others will be treated as unknown error
                    else -> GitHubErrorEntity.Unknown(throwable.message)
                }
            }
            else -> GitHubErrorEntity.Unknown(throwable.message)
        }
    }
}