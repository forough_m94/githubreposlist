package com.example.alibabatestproject.data.repository

import android.util.Log
import com.example.alibabatestproject.data.error.GitHubErrorHandler
import com.example.alibabatestproject.data.source.local.GitHubLocalDataSource
import com.example.alibabatestproject.data.source.local.model.UserRepoJoin
import com.example.alibabatestproject.data.source.local.model.toDetailDomain
import com.example.alibabatestproject.data.source.local.model.toSummaryDomain
import com.example.alibabatestproject.data.source.network.GitHubNetworkDataSource
import com.example.alibabatestproject.data.source.network.model.toDomain
import com.example.alibabatestproject.data.source.network.model.toLocal
import com.example.alibabatestproject.di.qualifier.IoDispatcherQualifier
import com.example.alibabatestproject.util.GitHubResult
import com.example.alibabatestproject.data.model.GitHubRepoDetailObject
import com.example.alibabatestproject.data.model.GitHubRepoSummaryObject
import com.example.alibabatestproject.data.model.GitHubUser
import com.example.alibabatestproject.util.SortType
import com.example.alibabatestproject.util.toResult
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.io.IOException
import javax.inject.Inject

class GitHubRepositoryImp @Inject constructor(
    private val localDataSource: GitHubLocalDataSource,
    private val networkNetworkDataSource: GitHubNetworkDataSource,
    private val errorHandler: GitHubErrorHandler,
    @IoDispatcherQualifier private val dispatcher: CoroutineDispatcher
) : GitHubRepository {

    /**
     * firs get repositories from local.
     * If there is not data, request to remote server and then update local DB.
     * If needRefresh == true, fetch data from server and if network in not connected returns local data
     */
    override fun getGitHubRepositories(
        page: Int,
        itemsPerPage: Int,
        sortType: SortType,
        needRefresh: Boolean
    ): Flow<GitHubResult<List<GitHubRepoSummaryObject>>> = flow {
        val localResult = localDataSource.getGithubRepositories(page, itemsPerPage, sortType)
        if (localResult.isEmpty() || needRefresh) {
            try {
                val networkData =
                    networkNetworkDataSource.getGitHubRepositories(page, itemsPerPage, sortType)
                if (needRefresh) {
                    localDataSource.deleteGithubRepositories()
                    localDataSource.deleteUsers()
                }
                localDataSource.insertUsers(networkData.map { it.owner.toLocal() })
                localDataSource.insertGithubRepositories(networkData.map { it.toLocal() })
                Log.d("emit data", "network data $needRefresh")
                emit(networkData.map { it.toDomain() })
            } catch (networkException: IOException) {
                Log.d("emit data", "network error ")
                if (needRefresh && localResult.isNotEmpty()) {
                    emit(localResult.map { it.toSummaryDomain() })
                }
                throw networkException
            }
        } else {
            Log.d("emit data", "local is not empty ")
            emit(localResult.map { it.toSummaryDomain() })
        }
    }.toResult(errorHandler)
        .flowOn(dispatcher)

    override suspend fun deleteGithubRepositories() {
        localDataSource.deleteGithubRepositories()
    }

    override fun getGitHubRepository(repoId: Int): Flow<GitHubResult<GitHubRepoDetailObject>> =
        flow {
            val userRepoPair = localDataSource.getGithubRepository(repoId)
            val repoOwner = userRepoPair.first
            val repo = userRepoPair.second
            emit(repo.toDetailDomain(repoOwner))
        }.toResult(errorHandler)

    /**
     * firs get contributors from local.
     * If there is not data, request to remote server and then update local DB.
     */
    override fun getContributors(
        repoId: Int,
        repoName: String,
        ownerName: String
    ): Flow<GitHubResult<List<GitHubUser>>> = flow {
        val localData = localDataSource.getUsersInRepository(repoId)
        if (localData.isEmpty()) {
            val remoteData = networkNetworkDataSource.getContributors(ownerName, repoName)
            localDataSource.insertUsers(remoteData.map { it.toLocal() })
            val userRepoJoins: ArrayList<UserRepoJoin> = arrayListOf()
            remoteData.forEach {
                userRepoJoins.add(UserRepoJoin(it.id, repoId))
            }
            localDataSource.insertAllUserRepoJoins(userRepoJoins)
            emit(remoteData.map { it.toDomain() })
        } else {
            emit(localData.map { it.toDetailDomain() })
        }
    }.toResult(errorHandler)

}
