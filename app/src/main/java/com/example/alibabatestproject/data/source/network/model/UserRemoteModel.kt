package com.example.alibabatestproject.data.source.network.model

import com.example.alibabatestproject.data.source.local.model.UserRoomModel
import com.example.alibabatestproject.data.model.GitHubUser
import com.google.gson.annotations.SerializedName

data class GitHubUserRemoteModel(
    @SerializedName("id") val id: Int,
    @SerializedName("login") val name: String?,
    @SerializedName("avatar_url") val avatarUrl: String?
)

fun GitHubUserRemoteModel.toDomain() = GitHubUser(
    id = id,
    name = name ?: "",
    avatarUrl = avatarUrl ?: ""
)

fun GitHubUserRemoteModel.toLocal() = UserRoomModel(
    id = id,
    name = name ?: "",
    avatarUrl = avatarUrl ?: ""
)

