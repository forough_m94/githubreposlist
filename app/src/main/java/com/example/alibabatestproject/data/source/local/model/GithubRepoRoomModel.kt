package com.example.alibabatestproject.data.source.local.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import com.example.alibabatestproject.data.model.GitHubRepoDetailObject
import com.example.alibabatestproject.data.model.GitHubRepoSummaryObject


@Entity(
    foreignKeys = [ForeignKey(
        entity = UserRoomModel::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("ownerId"),
        onDelete = CASCADE
    )]
)
data class GithubRepoRoomModel(
    @PrimaryKey
    val id: Int,
    val name: String,
    val starsCount: Int,
    val forksCount: Int,
    val description: String,
    val htmlUrl: String,
    val ownerId: Int,
    val contributorsUrl: String
)


fun GithubRepoRoomModel.toDetailDomain(user: UserRoomModel): GitHubRepoDetailObject {
    return GitHubRepoDetailObject(
        id = id,
        name = name,
        starsCount = starsCount,
        forksCount = forksCount,
        description = description,
        htmlUrl = htmlUrl,
        owner = user.toDetailDomain()
    )
}

fun GithubRepoRoomModel.toSummaryDomain(): GitHubRepoSummaryObject {
    return GitHubRepoSummaryObject(
        id = id,
        name = name,
        starsCount = starsCount,
        forksCount = forksCount,
        description = description,
        htmlUrl = htmlUrl,
        ownerId = ownerId
    )
}