package com.example.alibabatestproject.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.alibabatestproject.data.source.local.GitHubDao
import com.example.alibabatestproject.data.source.local.model.GithubRepoRoomModel
import com.example.alibabatestproject.data.source.local.model.UserRepoJoin
import com.example.alibabatestproject.data.source.local.model.UserRoomModel

@Database(
    entities = [GithubRepoRoomModel::class, UserRoomModel::class, UserRepoJoin::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun gitHubDao(): GitHubDao
}