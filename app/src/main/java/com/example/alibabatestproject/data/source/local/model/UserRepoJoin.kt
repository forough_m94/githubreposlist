package com.example.alibabatestproject.data.source.local.model

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    primaryKeys = ["userId", "repoId"],
    foreignKeys = [ForeignKey(
        entity = UserRoomModel::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("userId"),
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    ), ForeignKey(
        entity = GithubRepoRoomModel::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("repoId"),
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )]
)
data class UserRepoJoin(val userId: Int, val repoId: Int)