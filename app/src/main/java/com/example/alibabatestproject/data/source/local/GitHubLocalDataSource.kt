package com.example.alibabatestproject.data.source.local

import com.example.alibabatestproject.data.source.local.model.GithubRepoRoomModel
import com.example.alibabatestproject.data.source.local.model.UserRepoJoin
import com.example.alibabatestproject.data.source.local.model.UserRoomModel
import com.example.alibabatestproject.util.SortType

interface GitHubLocalDataSource {

    suspend fun insertUsers(users: List<UserRoomModel>)

    suspend fun insertAllUserRepoJoins(userRepoJoins: List<UserRepoJoin>)

    suspend fun getUsersInRepository(repoId: Int): List<UserRoomModel>

    suspend fun insertGithubRepositories(githubRepos: List<GithubRepoRoomModel>)

    suspend fun getGithubRepositories(
        page: Int,
        itemsPerPage: Int,
        sortType: SortType
    ): List<GithubRepoRoomModel>

    suspend fun getGithubRepository(repoId: Int): Pair<UserRoomModel, GithubRepoRoomModel>

    suspend fun deleteUsers()

    suspend fun deleteGithubRepositories()
}
