package com.example.alibabatestproject.data.source.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.alibabatestproject.data.model.GitHubUser

@Entity
data class UserRoomModel(
    @PrimaryKey
    val id: Int,
    val name: String,
    val avatarUrl: String
)

fun UserRoomModel.toDetailDomain(): GitHubUser {
    return GitHubUser(
        id = id,
        name = name,
        avatarUrl = avatarUrl
    )
}