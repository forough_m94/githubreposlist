package com.example.alibabatestproject.api

import com.example.alibabatestproject.data.source.network.model.GitHubUserRemoteModel
import com.example.alibabatestproject.data.model.GitHubRepositoriesOutput
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GitHubApi {

    companion object {
        const val GitHubBaseURL = "https://api.github.com/"
    }

    @GET("search/repositories?q=language:Kotlin")
    suspend fun getGitHubRepos(
        @Query("sort") sortBy: String,
        @Query("page") page: Int,
        @Query("per_page") itemsPerPage: Int
    ): GitHubRepositoriesOutput

    @GET("repos/{owner}/{repo}/contributors")
    suspend fun getRepositoryContributors(
        @Path("owner") owner: String,
        @Path("repo") repoName: String
    ): List<GitHubUserRemoteModel>
}