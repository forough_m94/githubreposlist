package com.example.alibabatestproject.data.source.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.filters.SmallTest
import com.example.alibabatestproject.data.source.local.model.GithubRepoRoomModel
import com.example.alibabatestproject.data.source.local.model.UserRepoJoin
import com.example.alibabatestproject.data.source.local.model.UserRoomModel
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject
import javax.inject.Named

@HiltAndroidTest
@SmallTest
class GithubLocalDataSourceImpTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    @Named("test_db")
    lateinit var database: AppDatabase
    private lateinit var gitHubDao: GitHubDao

    @Before
    fun setup() {
        hiltRule.inject()
        gitHubDao = database.gitHubDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    private val user1 = UserRoomModel(1, "a", "")
    private val user2 = UserRoomModel(2, "b", "")
    private val repoRoom1 = GithubRepoRoomModel(1, "", 1, 2, "", "", 1, "")
    private val repoRoom2 = GithubRepoRoomModel(2, "", 2, 1, "", "", 1, "")

    @Test
    fun testInsertUsers() = runBlocking {
        val users = arrayListOf(user1)
        gitHubDao.insertAllUsers(users)
        val fetchedUsers = gitHubDao.getUser(1)
        assertEquals(user1.id, fetchedUsers.id)
    }

    @Test
    fun testInsertAllUserRepoJoins() = runBlocking {
        val users = arrayListOf(user1, user2)
        gitHubDao.insertAllUsers(users)
        gitHubDao.insertAllRepositories(arrayListOf(repoRoom1, repoRoom2))
        gitHubDao.insertAllUserRepoJoins(arrayListOf(UserRepoJoin(user2.id, repoRoom1.id)))
        val contributors = gitHubDao.getUsersInRepository(repoRoom1.id)
        assertEquals(user2.id, contributors[0].id)
    }

    @Test
    fun testInsertGithubRepositories() = runBlocking {
        gitHubDao.insertAllUsers(arrayListOf(user1))
        gitHubDao.insertAllRepositories(arrayListOf(repoRoom1))
        val repo = gitHubDao.getGitHubRepository(repoRoom1.id)
        assertEquals(repoRoom1.id, repo.id)
    }

    @Test
    fun testDeleteUsers() = runBlocking {
        gitHubDao.insertAllUsers(arrayListOf(user1))
        gitHubDao.deleteUsers()
        assertEquals(null, gitHubDao.getUser(user1.id))
    }

    @Test
    fun testDeleteGithubRepositories() = runBlocking {
        gitHubDao.insertAllUsers(arrayListOf(user1))
        gitHubDao.insertAllRepositories(arrayListOf(repoRoom1))
        gitHubDao.deleteGithubRepositories()
        assertEquals(null, gitHubDao.getGitHubRepository(repoRoom1.id))
    }
}