package com.example.alibabatestproject.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.alibabatestproject.data.error.GitHubErrorHandler
import com.example.alibabatestproject.data.source.local.GitHubLocalDataSource
import com.example.alibabatestproject.data.source.local.model.GithubRepoRoomModel
import com.example.alibabatestproject.data.source.local.model.UserRoomModel
import com.example.alibabatestproject.data.source.local.model.toSummaryDomain
import com.example.alibabatestproject.data.source.network.GitHubNetworkDataSource
import com.example.alibabatestproject.data.source.network.model.GitHubRepoRemoteModel
import com.example.alibabatestproject.data.source.network.model.GitHubUserRemoteModel
import com.example.alibabatestproject.data.source.network.model.toDomain
import com.example.alibabatestproject.util.GitHubResult
import com.example.alibabatestproject.fake.FakeLocalDataSource
import com.example.alibabatestproject.fake.FakeNetworkDataSource
import com.example.alibabatestproject.util.SortType
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class GitHubRepositoryImpTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()


    private lateinit var fakeLocalDataSource: GitHubLocalDataSource

    private lateinit var fakeNetworkDataSource: GitHubNetworkDataSource

    @Mock
    private lateinit var mockErrorHandler: GitHubErrorHandler

    private lateinit var gitHubRepository: GitHubRepository

    private lateinit var localRepoData: MutableList<GithubRepoRoomModel>
    private lateinit var localUserData: MutableList<UserRoomModel>
    private lateinit var remoteRepoData: MutableList<GitHubRepoRemoteModel>
    private lateinit var remoteUserData: MutableList<GitHubUserRemoteModel>

    @Before
    fun setUp() {
        addFakeRepoToLocal()
        addFakeRepoToRemote()
        addFakeUserToLocal()
        addFakeUserToRemote()

        fakeLocalDataSource = FakeLocalDataSource(
            userList = localUserData,
            repoList = localRepoData
        )

        fakeNetworkDataSource = FakeNetworkDataSource(
            users = remoteUserData,
            repos = remoteRepoData
        )

        gitHubRepository = GitHubRepositoryImp(
            localDataSource = fakeLocalDataSource,
            networkNetworkDataSource = fakeNetworkDataSource,
            errorHandler = mockErrorHandler,
            dispatcher = Dispatchers.Unconfined
        )
    }

    @Test
    fun getGitHubRepositories_nonEmptyLocalAndForceDisableSortByStarCount() =
        runBlockingTest {
            // GIVEN
            val force = false
            val itemPerPage = 10
            val sortType = SortType.STARS
            val page = 1

            // WHEN
            val repos = gitHubRepository.getGitHubRepositories(
                page = page,
                itemsPerPage = itemPerPage,
                sortType = sortType,
                needRefresh = force
            )

            // THEN
            val result = repos.toList()

            assertEquals(result.first(), GitHubResult.Loading)

            val expected = GitHubResult.Success(
                fakeLocalDataSource.getGithubRepositories(
                    page, itemPerPage, sortType
                ).map { it.toSummaryDomain() }
            )
            assertEquals(expected, result.last())
        }


    @Test
    fun getGitHubRepositories_nonEmptyLocalAndForceEnabledSortByStarCount() =
        runBlockingTest {
            // GIVEN
            val itemPerPage = 10
            val sortType = SortType.STARS
            val force = true
            val page = 1

            // WHEN
            val repos = gitHubRepository.getGitHubRepositories(
                page = page,
                itemsPerPage = itemPerPage,
                sortType = sortType,
                needRefresh = force
            )

            // THEN
            val result = repos.toList()

            assertEquals(result.first(), GitHubResult.Loading)

            val expected = GitHubResult.Success(
                fakeNetworkDataSource.getGitHubRepositories(
                    page,
                    itemPerPage,
                    sortType
                ).map { it.toDomain() }
            )
            assertEquals(expected, result.last())
        }

    @Test
    fun getGitHubRepositories_EmptyLocalAndForceDisableSortByStarCount() = runBlockingTest {
        // GIVEN
        fakeLocalDataSource.deleteGithubRepositories()
        val itemPerPage = 10
        val sortType = SortType.STARS
        val force = false
        val page = 1

        // WHEN
        val repos = gitHubRepository.getGitHubRepositories(
            page = page,
            itemsPerPage = itemPerPage,
            sortType = sortType,
            needRefresh = force
        )

        // THEN
        val result = repos.toList()

        assertEquals(result.first(), GitHubResult.Loading)

        val expected = GitHubResult.Success(
            fakeNetworkDataSource.getGitHubRepositories(
                page,
                itemPerPage,
                sortType
            ).map { it.toDomain() })
        assertEquals(expected, result.last())

    }

    @Test
    fun getGitHubRepositories_EmptyLocalAndForceEnableSortByStarCount() = runBlockingTest {
        // GIVEN
        fakeLocalDataSource.deleteGithubRepositories()
        val itemPerPage = 10
        val sortType = SortType.STARS
        val force = true
        val page = 1

        // WHEN
        val repos = gitHubRepository.getGitHubRepositories(
            page = page,
            itemsPerPage = itemPerPage,
            sortType = sortType,
            needRefresh = force
        )

        // THEN
        val result = repos.toList()

        assertEquals(result.first(), GitHubResult.Loading)

        val expected = GitHubResult.Success(
            fakeNetworkDataSource.getGitHubRepositories(
                page,
                itemPerPage,
                sortType
            ).map { it.toDomain() })
        assertEquals(expected, result.last())
    }

    @Test
    fun getGitHubRepositories_nonEmptyLocalAndForceDisableSortByForkCount() =
        runBlockingTest {
            // GIVEN
            val force = false
            val itemPerPage = 10
            val sortType = SortType.FORKS
            val page = 1

            // WHEN
            val repos = gitHubRepository.getGitHubRepositories(
                page = page,
                itemsPerPage = itemPerPage,
                sortType = sortType,
                needRefresh = force
            )

            // THEN
            val result = repos.toList()

            assertEquals(result.first(), GitHubResult.Loading)

            val expected = GitHubResult.Success(fakeLocalDataSource.getGithubRepositories(
                page, itemPerPage, sortType
            ).map { it.toSummaryDomain() })
            assertEquals(expected, result.last())
        }


    @Test
    fun getGitHubRepositories_nonEmptyLocalAndForceEnabledSortByForkCount() =
        runBlockingTest {
            // GIVEN
            val itemPerPage = 10
            val sortType = SortType.FORKS
            val force = true
            val page = 1

            // WHEN
            val repos = gitHubRepository.getGitHubRepositories(
                page = page,
                itemsPerPage = itemPerPage,
                sortType = sortType,
                needRefresh = force
            )

            // THEN
            val result = repos.toList()

            assertEquals(result.first(), GitHubResult.Loading)

            val expected = GitHubResult.Success(
                fakeNetworkDataSource.getGitHubRepositories(
                    page,
                    itemPerPage,
                    sortType
                ).map { it.toDomain() })
            assertEquals(expected, result.last())
        }

    @Test
    fun getGitHubRepositories_EmptyLocalAndForceDisableSortByForkCount() = runBlockingTest {
        // GIVEN
        fakeLocalDataSource.deleteGithubRepositories()

        val itemPerPage = 10
        val sortType = SortType.STARS
        val force = false
        val page = 1

        // WHEN
        val repos = gitHubRepository.getGitHubRepositories(
            page = page,
            itemsPerPage = itemPerPage,
            sortType = sortType,
            needRefresh = force
        )

        // THEN
        val result = repos.toList()

        assertEquals(result.first(), GitHubResult.Loading)

        val expected = GitHubResult.Success(
            fakeNetworkDataSource.getGitHubRepositories(
                page,
                itemPerPage,
                sortType
            ).map { it.toDomain() })
        assertEquals(expected, result.last())
    }

    @Test
    fun getGitHubRepositories_EmptyLocalAndForceEnableSortByForkCount() = runBlockingTest {
        // GIVEN
        fakeLocalDataSource.deleteGithubRepositories()

        val itemPerPage = 10
        val sortType = SortType.STARS
        val force = true
        val page = 1

        // WHEN
        val repos = gitHubRepository.getGitHubRepositories(
            page = page,
            itemsPerPage = itemPerPage,
            sortType = sortType,
            needRefresh = force
        )

        // THEN
        val result = repos.toList()

        assertEquals(result.first(), GitHubResult.Loading)

        val expected = GitHubResult.Success(
            fakeNetworkDataSource.getGitHubRepositories(
                page,
                itemPerPage,
                sortType
            ).map { it.toDomain() })
        assertEquals(expected, result.last())
    }

    // initialization
    private fun addFakeRepoToRemote() {
        remoteRepoData = mutableListOf()
        val fakes = (1000..1999).map {
            GitHubRepoRemoteModel(
                id = it,
                name = "name #$it",
                starsCount = it,
                forksCount = 2000 - it,
                description = "description #$it",
                htmlUrl = "",
                owner = GitHubUserRemoteModel(
                    id = it % 10,
                    name = "name #${it % 10}",
                    avatarUrl = ""
                ),
                contributorsUrl = ""
            )
        }
        remoteRepoData.addAll(fakes)
    }

    private fun addFakeRepoToLocal() {
        localRepoData = mutableListOf()
        val fakes = (0..999).map {
            GithubRepoRoomModel(
                id = it,
                name = "name #$it",
                starsCount = it,
                forksCount = 2000 - it,
                description = "description #$it",
                htmlUrl = "",
                ownerId = it % 10,
                contributorsUrl = ""
            )
        }
        localRepoData.addAll(fakes)
    }

    private fun addFakeUserToLocal() {
        localUserData = mutableListOf()
        val fakes = (0..9).map {
            UserRoomModel(
                id = it,
                name = "name #$it",
                avatarUrl = ""
            )
        }
        localUserData.addAll(fakes)
    }

    private fun addFakeUserToRemote() {
        remoteUserData = mutableListOf()
        val fakes = (0..9).map {
            GitHubUserRemoteModel(
                id = it,
                name = "name #$it",
                avatarUrl = ""
            )
        }
        remoteUserData.addAll(fakes)
    }
}