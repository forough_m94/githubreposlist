package com.example.alibabatestproject.fake

import com.example.alibabatestproject.data.source.network.GitHubNetworkDataSource
import com.example.alibabatestproject.data.source.network.model.GitHubRepoRemoteModel
import com.example.alibabatestproject.data.source.network.model.GitHubUserRemoteModel
import com.example.alibabatestproject.util.SortType

class FakeNetworkDataSource(
    private val users: List<GitHubUserRemoteModel>,
    private val repos: List<GitHubRepoRemoteModel>
) : GitHubNetworkDataSource {
    override suspend fun getGitHubRepositories(
        page: Int,
        itemsPerPage: Int,
        sortType: SortType
    ): List<GitHubRepoRemoteModel> = when (sortType) {
        SortType.STARS -> repos
            .sortedByDescending { it.starsCount }
            .drop((page - 1) * itemsPerPage)
            .take(itemsPerPage)
        SortType.FORKS -> repos
            .sortedByDescending { it.forksCount }
            .drop((page - 1) * itemsPerPage)
            .take(itemsPerPage)

    }

    override suspend fun getContributors(
        owner: String,
        repoName: String
    ): List<GitHubUserRemoteModel> {
        TODO("Not yet implemented")
    }
}