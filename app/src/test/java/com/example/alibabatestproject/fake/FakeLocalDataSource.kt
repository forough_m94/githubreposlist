package com.example.alibabatestproject.fake

import com.example.alibabatestproject.data.source.local.GitHubLocalDataSource
import com.example.alibabatestproject.data.source.local.model.GithubRepoRoomModel
import com.example.alibabatestproject.data.source.local.model.UserRepoJoin
import com.example.alibabatestproject.data.source.local.model.UserRoomModel
import com.example.alibabatestproject.util.SortType

class FakeLocalDataSource(
    private val userList: MutableList<UserRoomModel> = mutableListOf(),
    private val repoList: MutableList<GithubRepoRoomModel> = mutableListOf()
) : GitHubLocalDataSource {
    override suspend fun insertUsers(users: List<UserRoomModel>) {
        userList.addAll(users)
    }

    override suspend fun insertAllUserRepoJoins(userRepoJoins: List<UserRepoJoin>) {
        TODO("Not yet implemented")
    }

    override suspend fun getUsersInRepository(repoId: Int): List<UserRoomModel> {
        TODO("Not yet implemented")
    }

    override suspend fun insertGithubRepositories(githubRepos: List<GithubRepoRoomModel>) {
        repoList.addAll(githubRepos)
    }

    override suspend fun getGithubRepositories(
        page: Int,
        itemsPerPage: Int,
        sortType: SortType
    ): List<GithubRepoRoomModel> = when (sortType) {
        SortType.STARS -> repoList
            .sortedByDescending { it.starsCount }
            .drop((page - 1) * itemsPerPage)
            .take(itemsPerPage)
        SortType.FORKS -> repoList
            .sortedByDescending { it.forksCount }
            .drop((page - 1) * itemsPerPage)
            .take(itemsPerPage)
    }

    override suspend fun getGithubRepository(repoId: Int): Pair<UserRoomModel, GithubRepoRoomModel> {
        TODO("Not yet implemented")
    }

    override suspend fun deleteUsers() {
        userList.clear()
    }

    override suspend fun deleteGithubRepositories() {
        repoList.clear()
    }
}